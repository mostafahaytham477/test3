/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;
import static java.lang.Math.*;
import java.util.Scanner;

/**
 *
 * @author bb
 */
public class JavaApplication1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.print("Enter your intial number:");
        Scanner s=new Scanner (System.in);
        double x[]=new double[100];
        x[0] = s.nextDouble();
        System.out.print("Solve the equation with (1.Newton-Raphson/2.Fixed point)");
        int mychoice=s.nextInt();
        if (mychoice == 1) {
            for (int i = 0; i < 100; i++) {
                double upper = (5 * x[i]) - (exp(-x[i])) + (sin(toRadians(x[i])));
                System.out.println("Upper: " + upper);
                double down = 5 + exp(-x[i]) + cos(toRadians(x[i]));
                System.out.println("down: " + down);
                x[i + 1] = x[i] - (upper / down);
                System.out.println("X" + (i + 1) + ": " + x[i + 1]);
                double error = abs((((x[i + 1] - x[i]) / x[i + 1]) * 100));
                System.out.println("Error: " + error);
                if (error < 0.05) {
                    break;
                }
            }
        }
        else if(mychoice==2)
        {
            for (int i = 0; i < 100; i++) {
                 x[i+1] =  ((exp(-x[i])) - (sin(toRadians(x[i]))))/5;
                System.out.println("X" + (i + 1) + ": " + x[i + 1]);
                double error = abs((((x[i + 1] - x[i]) / x[i + 1]) * 100));
                System.out.println("Error: " + error);
                if (error < 0.05) {
                    break;
                }
            }
        }
    }
    
}
